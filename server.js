// server.js
// load the things we need
var express = require('express')
var juice = require('juice')
var fs = require('fs')
var app = express()
var data = require('./data.json')
let filePath = __dirname + '/views/index.ejs'
let options = { preserveMediaQueries: false }
app.use('/', express.static(__dirname + '/public'))

var result = juice.juiceFile(filePath, options, function(err, html) {
  if (err) console.log('err', err)

  fs.writeFile(__dirname + '/inline/index.html', html, function(err) {
    if (err) {
      return console.log('write Err' + err)
    } else {
      console.log('The file was saved!')
    }
  })
})

// set the view engine to ejs
app.set('view engine', 'ejs')

// index page
app.get('/', function(req, res) {
  res.render('index', {
    orderData: data,
    airport_city: [
      {
        number: 3670,
        FullName: 'Dallas Fort Worth International Airport',
        Name: 'Dallas-Fort Worth',
        Country: 'United States',
        IATA: 'DFW',
        ICAO: 'KDFW',
        Latitude: 32.89680099487305,
        Longitude: -97.03800201416016,
        Altitude: 607,
        Timezone: -6,
        DST: 'A',
        timezone: 'America/Chicago',
        Type: 'airport',
        Source: 'OurAirports',
        CountryCode2: 'US'
      },
      {
        number: 3484,
        FullName: 'Los Angeles International Airport',
        Name: 'Los Angeles',
        Country: 'United States',
        IATA: 'LAX',
        ICAO: 'KLAX',
        Latitude: 33.94250107,
        Longitude: -118.4079971,
        Altitude: 125,
        Timezone: -8,
        DST: 'A',
        timezone: 'America/Los_Angeles',
        Type: 'airport',
        Source: 'OurAirports',
        CountryCode2: 'US'
      },
      {
        number: 2428,
        FullName: 'Guiuan Airport',
        Name: 'Guiuan',
        Country: 'Philippines',
        IATA: '\\N',
        ICAO: 'RPVG',
        Latitude: 11.0354995728,
        Longitude: 125.741996765,
        Altitude: 7,
        Timezone: 8,
        DST: 'N',
        timezone: 'Asia/Manila',
        Type: 'airport',
        Source: 'OurAirports',
        CountryCode2: 'PH'
      },
      {
        number: 3406,
        FullName: 'Shanghai Pudong International Airport',
        Name: 'Shanghai',
        Country: 'China',
        IATA: 'PVG',
        ICAO: 'ZSPD',
        Latitude: 31.143400192260742,
        Longitude: 121.80500030517578,
        Altitude: 13,
        Timezone: 8,
        DST: 'U',
        timezone: 'Asia/Shanghai',
        Type: 'airport',
        Source: 'OurAirports',
        CountryCode2: 'CN'
      },
      {
        number: 3830,
        FullName: "Chicago O'Hare International Airport",
        Name: 'Chicago',
        Country: 'United States',
        IATA: 'ORD',
        ICAO: 'KORD',
        Latitude: 41.97859955,
        Longitude: -87.90480042,
        Altitude: 672,
        Timezone: -6,
        DST: 'A',
        timezone: 'America/Chicago',
        Type: 'airport',
        Source: 'OurAirports',
        CountryCode2: 'US'
      }
    ]
  })
})

app.listen(3005)
console.log('listening 3005')
